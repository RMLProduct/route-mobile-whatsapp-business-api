### The WhatsApp Business Solution(WBS) empowers a way to connect your customers across the globe on WhatsApp platform in a simple, secure, and reliable manner.

For API Token and registration please contact whatsapp@routemobile.com

***

### Onboarding
Please download and fill the form and send on whatsapp@routemobile.com

[On-boarding Form](https://drive.google.com/file/d/1j2ahCtlstzd5Eq3EnhSaczsfYtQf44Ct/view?usp=sharing)
***

### Requirement 
* Valid Phone Number capable for receiving the OTP
* Facebook manager ID 
* Brand guidelines and document 
 


***


### API Installation 
Use Postman for API for import and run the collection


***

### API Type
**Notification Messages**
* Brand initiated messages
* Needs pre-approved message templates
* Send rich communication like text, images, documents, and video files
* Needs an Active Opt-in

**Session Messages**
* A User-initiated message always starts a Session 
* A Session lasts for 24 hours from the most recently received user message 
* Send text, images, documents, videos, audio files, locations
* No Opt-in and No Pre-approved templates needed 


***


### Commerical 
**Notification Commercials**
* Per message fee based on terminating number country code
* The fee remains the same whether its text or rich media and applies to all Mobile Terminated messages (MT)


**Session Commercials**
* Route Mobile charges a fixed fee for every User initiated (MO) and Brand Response (MT) in a Session
* A session lasts for 24 hours from the most recent MO message


***


### License 

Please refer to the Whatsapp@routemobile.com




 




